import { DWNClient, MessageStorage, ThreadMethod } from "@quarkid/dwn-client";
import { Entry } from '@quarkid/dwn-client/dist/types/message';
import { buildCredential, getDid, getOrCreateKeys, kmsFactory, resolveDid } from "./common";
import { IDIDCommMessage } from "@extrimian/kms-core";
import { VerifiableCredential, VerifiableCredentialService } from "@quarkid/vc-core";
import { Suite } from "@quarkid/kms-core";
import { AssertionMethodPurpose, DIDDocumentUtils, VerificationMethodJwk } from "@quarkid/did-core";
import { KMSClient } from "@quarkid/kms-client";

import {
    BaseConverter,
    Base,
  } from "@extrimian/kms-core";

let messagesStorage: Entry[] = [];
let lastPullDate: Date = new Date();

export const storageMock: MessageStorage = {

    async getMessages(): Promise<Entry[]> {
        return messagesStorage;
    },
    async getLastPullDate(): Promise<Date> {
        return lastPullDate;
    },
    async saveMessages(messages: Entry[]): Promise<void> {
        messagesStorage.push(...messages);
    },
    async updateLastPullDate(date: Date): Promise<void> {
        lastPullDate = date;
    },
  };

const sendMsg = async (emisor: string, target: string, packedMessage) => {

    const didDocument = await resolveDid(target);
    console.log("DID Document is:", didDocument);
    if (!didDocument) {
        console.log("No DID Document found for target");
        process.exit(1);
    }
    console.log("Service is:", didDocument.service);
    const service = DIDDocumentUtils.getServiceUrl(didDocument, "DecentralizedWebNode", "nodes");
    if (!service) {
        console.log("Services", service);
        console.log("No service found in DID Document");
        process.exit(1);
    }
    console.log("Service found in DID Document", service[0]);
    const url = service[0];

    const dwnClient = new DWNClient({
      did : emisor,
      inboxURL : url,
      storage : storageMock,
    });
    let ret = await dwnClient.sendMessage({
        targetDID: target, // DID del destinatario
        targetInboxURL: url, // refiere al servicio del dwn del destinatario, el mismo se obtiene resolviendo el DidDocument
        message: {
          data: packedMessage, //el mensaje a enviar debe ir empaquetado con DidComm
          descriptor: {
            method: ThreadMethod.Create,
            dateCreated: new Date(),
            dataFormat: "application/json",
          },
        },
    });

    console.log("Returning sendMessage: ", ret);
    return ret;
}

let kms:KMSClient;
let sendMe = async (profileID: string, didTarget: string, credential: Promise<VerifiableCredential>) => {
    kms = await kmsFactory(profileID);
    let didEmisor = await getDid();
    if (!didEmisor) {
        console.log("No DID found for profile.");
        process.exit(1);
    }
    //console.log("vc signed: ", credential);

    let message:IDIDCommMessage = {
        type: "application/json",
        body: await credential,
        from: didEmisor,
        to: [didTarget],
        id: "1234567890",
    };

    // console.log("Verifiable Credential Signed", credential, message);
    const packedMessage = await kms.packDIDCommV2({
        senderVerificationMethodId:
            didEmisor + "#didComm", //Verification method del emisor del mensaje
        recipientVerificationMethodIds: [
            didTarget + "#didComm"
        ], //Verification method del destinatario del mensaje, puede haber mas de un destinatario
        message: message, //Mensage de tipo DIDComm
        packing: "authcrypt",
    });
    console.log(packedMessage);
    console.log(await sendMsg(didEmisor, didTarget, packedMessage));
    console.log(messagesStorage);
};

// tomo el profile de argumentos de consola
const profileID = process.argv[2];
const didTarget = process.argv[3];
if (!profileID || !didTarget) {
    console.log("Usage: npx tsx send-credential.ts <profileID> <didTarget>");
    process.exit(1);
}

kmsFactory(profileID);
let credential = buildCredential();
sendMe(profileID, didTarget, credential);
