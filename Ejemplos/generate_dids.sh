#!/bin/bash

# Define the endpoint and header
endpoint="http://api-zksync-recetas-bc:8000/create"
header="Content-Type: application/json"

# Define valid multihashes for each request
data1='{"type":"create","suffixData":{"deltaHash":"EiA0dT0L8kltGf5_m4PCfKc4tGDD5alxsv16vMPxcNIMCg","recoveryCommitment":["EiBNSzlldPYgMW99gw64BOqWnROycD6593ndLQmZZ4l3aA"]},"delta":{"updateCommitment":["EiDwrbcS-ei5_hcnTsR35zUgzcW9hrgmilm14ZKoFcOpSA"],"patches":[{"action":"replace","document":{"publicKeys":[{"id":"bbsbls1","publicKeyJwk":{"kty":"EC","crv":"Bls12381G2Key2020","x":"i8YRYFsUhlfnennwSZnV0v5s1NJ8Tx0L3WXrgtmSfHQOCEO3T2qeAgYj6YrcJ2o2","y":"BV7n0VNKyMzZnZ5-zQNsojQJ4x2Z1YO2jClGTTJlX53RhLjqAo__KUJ22c5PipiF"},"type":"Bls12381G1Key2020","purposes":["assertionMethod"]},{"id":"didComm1","publicKeyJwk":{"kty":"EC","crv":"ed25519","x":"kO6bria07wfp3-2hLjgaEw","y":"-SBFzntff-9MMOMtaJ23jQ"},"type":"X25519KeyAgreementKey2019","purposes":["keyAgreement"]}],"services":[{"id":"dwn-default","type":"DecentralizedWebNode","serviceEndpoint":{"nodes":["https://dwn-ssi.buenosaires.gob.ar/"]}}]}}]}}'

data2='{"type":"create","suffixData":{"deltaHash":"EiD9CejH2FbXZHsD2V6uNOdRcbR-P3pZZHyr-LQX_yZx7Q","recoveryCommitment":["EiBNSzlldPYgMW99gw64BOqWnROycD6593ndLQmZZ4l3aA"]},"delta":{"updateCommitment":["EiDwrbcT-ei5_hcnTsR35zUgzcW9hrgmilm14ZKoFcOpSB"],"patches":[{"action":"replace","document":{"publicKeys":[{"id":"bbsbls2","publicKeyJwk":{"kty":"EC","crv":"Bls12381G2Key2020","x":"i8YRYFsUhlfnennwSZnV0v5s1NJ8Tx0L3WXrgtmSfHQOCEO3T2qeAgYj6YrcJ2o2","y":"BV7n0VNKyMzZnZ5-zQNsojQJ4x2Z1YO2jClGTTJlX53RhLjqAo__KUJ22c5PipiF"},"type":"Bls12381G1Key2020","purposes":["assertionMethod"]},{"id":"didComm2","publicKeyJwk":{"kty":"EC","crv":"ed25519","x":"kO6bria07wfp3-2hLjgaEw","y":"-SBFzntff-9MMOMtaJ23jQ"},"type":"X25519KeyAgreementKey2019","purposes":["keyAgreement"]}],"services":[{"id":"dwn-default","type":"DecentralizedWebNode","serviceEndpoint":{"nodes":["https://dwn-ssi.buenosaires.gob.ar/"]}}]}}]}}'

# Make the requests
curl -X POST $endpoint -H "$header" -d "$data1"
curl -X POST $endpoint -H "$header" -d "$data2"

