* Obtener información sobre un bloque específico:

> Primer bloque recetasbc: 2672123

~~~
docker exec -it api-zksync-rbc curl -X POST https://sepolia.era.zksync.dev \
-H "Content-Type: application/json" \
-d '{
  "jsonrpc": "2.0",
  "method": "eth_getBlockByNumber",
  "params": ["0x28c5fb", true],
  "id": 1
}'
~~~

~~~json
{
   "jsonrpc":"2.0",
   "result":{
      "baseFeePerGas":"0x79f436a",
      "difficulty":"0x0",
      "extraData":"0x",
      "gasLimit":"0x4000000000000",
      "gasUsed":"0x27fd51",
      "hash":"0xa3e1391a4ed6e6dbc7cbbb4dac8e181b957ce664adb86446df2ff28c63023671",
      "l1BatchNumber":"0x23e3",
      "l1BatchTimestamp":"0x665df69d",
      "logsBloom":"0x00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
      "miner":"0x0000000000000000000000000000000000000000",
      "mixHash":"0x0000000000000000000000000000000000000000000000000000000000000000",
      "nonce":"0x0000000000000000",
      "number":"0x28c5fb",
      "parentHash":"0xe3efe5dcd941fcd9aab8647fa3680a16fd67e793bee0733dd69fc303faff6a66",
      "receiptsRoot":"0x0000000000000000000000000000000000000000000000000000000000000000",
      "sealFields":[
         
      ],
      "sha3Uncles":"0x1dcc4de8dec75d7aab85b567b6ccd41ad312451b948a7413f0a142fd40d49347",
      "size":"0x0",
      "stateRoot":"0x0000000000000000000000000000000000000000000000000000000000000000",
      "timestamp":"0x665e0c81",
      "totalDifficulty":"0x0",
      "transactions":[
         {
            "blockHash":"0xa3e1391a4ed6e6dbc7cbbb4dac8e181b957ce664adb86446df2ff28c63023671",
            "blockNumber":"0x28c5fb",
            "chainId":"0x12c",
            "from":"0x09ffee3e51f0bea294220abe19f7dfb2a5d7fb6d",
            "gas":"0x52c6ce",
            "gasPrice":"0x79f436a",
            "hash":"0x3c04d735f92cdc380054252589ebbbcc560d21ac2c030a3d3c743ccc781f74b4",
            "input":"0x4cd27ad51dff3eebda21d1987a725e41e98f8f001ac798c2e50c6ab1460854633807d3060000000000000000000000000000000000000000000000000000000000000001",
            "l1BatchNumber":"0x23e3",
            "l1BatchTxIndex":"0x27c",
            "maxFeePerGas":"0x9c5892f",
            "maxPriorityFeePerGas":"0x9c5892f",
            "nonce":"0x0",
            "r":"0x3b8020347f2f871513057cd2773036535ed2174403222a32686ddcd59a33edea",
            "s":"0x55f7f4cab9652a55a5063c2e25aa3e02f954f346554c840effc2d25aa3f0337c",
            "to":"0x2535412fa22d9ad83384d7ab7b636dda37efa872",
            "transactionIndex":"0x0",
            "type":"0x0",
            "v":"0x27c",
            "value":"0x0"
         }
      ],
      "transactionsRoot":"0x0000000000000000000000000000000000000000000000000000000000000000",
      "uncles":[
         
      ]
   },
   "id":1
}
~~~

* Obtener logs de transacciones de una dirección específica.

~~~
docker exec -it api-zksync-rbc curl -X POST https://sepolia.era.zksync.dev \
-H "Content-Type: application/json" \
-d '{
  "jsonrpc": "2.0",
  "method": "eth_getLogs",
  "params": [{
    "address": "0x09ffEe3e51F0Bea294220ABe19f7dFB2a5D7FB6D",
    "fromBlock": "earliest",
    "toBlock": "latest"
  }],
  "id": 1
}'
~~~


# Obtener información sobre una transacción específica:

~~~
docker exec -it api-zksync-rbc curl -X POST https://sepolia.era.zksync.dev \
-H "Content-Type: application/json" \
-d '{
  "jsonrpc": "2.0",
  "method": "eth_getTransactionByHash",
  "params": ["0xYourTransactionHash"],
  "id": 1
}'
~~~