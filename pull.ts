import { DIDDocumentUtils } from "@quarkid/did-core";
import { getDid, kmsFactory, resolveDid, verify } from "./common";
import { MessageStorage } from "@quarkid/dwn-client";
import { Entry } from '@quarkid/dwn-client/dist/types/message';
import { DWNClient } from "@quarkid/dwn-client";


let messagesStorage: Entry[] = [];
let lastPullDate: Date = new Date();
// ayer
lastPullDate.setDate(lastPullDate.getDate() - 1);

export const storageMock: MessageStorage = {

    async getMessages(): Promise<Entry[]> {
        return messagesStorage;
    },
    async getLastPullDate(): Promise<Date> {
        return lastPullDate;
    },
    async saveMessages(messages: Entry[]): Promise<void> {
        messagesStorage.push(...messages);
    },
    async updateLastPullDate(date: Date): Promise<void> {
        lastPullDate = date;
    },
  };

const pullMsg = async (profileID: string) => {
    console.log("Pulling messages for profile: " + profileID + " ...");
    const kms = await kmsFactory(profileID);
    const did = await getDid();
    if (!did) {
        console.log("No DID found for profile.");
        process.exit(1);
    }

    const didDocument = await resolveDid(did);
    if (!didDocument) {
        console.log("No DID Document found for profile.");
        process.exit(1);
    }

    // busco el servicio
    const services = DIDDocumentUtils.getServiceUrl(didDocument, "DecentralizedWebNode", "nodes");
    if (!services) {
        console.log("No service found in DID Document");
        process.exit(1);
    }

    const url = services[0];

    // creo el cliente
    const dwnClient = new DWNClient({
        did: did,
        inboxURL: url,
        storage: storageMock,
    });

    dwnClient.pullNewMessageWait().then(() => {
        console.log(messagesStorage, lastPullDate);
        messagesStorage.forEach(async (msg) => {
            //console.log(msg.data.packedMessage);
            const unpackedMsg = await kms.unpackvDIDCommV2(
                did,
                msg.data.packedMessage
            );
            console.log("Unpacked message: ", unpackedMsg);
            let result = await verify(unpackedMsg.message.body);
            console.log("vc verified: ", result);
            //console.log("Unpacked message: ", unpackedMsg.message.body);
            
        });
    }).catch((e) => {
        console.error(e);
    });
    /*
    let countdown = 10;
    let interval = setInterval(() => {
        console.log("Last pull date: ", lastPullDate, --countdown);
        console.log("Messages: ", messagesStorage.length);
        if (countdown <= 0) {
            clearInterval(interval);
        }
    }, 500);
    */
};

// tomo el profile de argumentos de consola
let profileID = process.argv[2];
if (!profileID) {
    console.log("Usage: npx tsx pull.ts <profileID>");
    process.exit(1);
}

pullMsg(profileID);
