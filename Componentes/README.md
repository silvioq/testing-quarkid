## api-proxy.

Para generar la imagen del **api-proxy** se debe ejecutar ek siguiente comando.

~~~
docker build -f ./api-proxy/source/Dockerfile -t api-prory-recetasbc:v0.0.1 .
~~~

Para apuntar al nodo local con el que se agruparan las tx para que  luego sean agregadas a la Blockchain se editaron los siguientes archivos.

* api-proxy/source/enviroments/config.json

~~~
{
"NODE_1_URL":"http://api-zksync-recetas-bc:8000",
"NODE_1_PATTERN":"did:recetasbc",
"NODE_1_BEHAVIOR":1
}
~~~

* api-proxy/source/src/.env

~~~
NODE_1_URL='http://api-zksync-recetas-bc:8000'
NODE_1_PATTERN='did:recetasbc'
NODE_1_BEHAVIOR='1'
TZ='America/Argentina/Buenos_Aires'
~~~

## Test

~~~
 curl -v http://api-proxy-rbc:8080/mappings
~~~

~~~
* Host api-proxy-rbc:8080 was resolved.
* IPv6: (none)
* IPv4: 127.0.0.1
*   Trying 127.0.0.1:8080...
* Connected to api-proxy-rbc (127.0.0.1) port 8080
> GET /mappings HTTP/1.1
> Host: api-proxy-rbc:8080
> User-Agent: curl/8.6.0
> Accept: */*
> 
< HTTP/1.1 200 OK
< X-Powered-By: Express
< Content-Type: application/json; charset=utf-8
< Content-Length: 93
< ETag: W/"5d-MVcKofhVlVlhmCLXbL3RRMySUtU"
< Date: Thu, 30 May 2024 18:06:53 GMT
< Connection: keep-alive
< Keep-Alive: timeout=5
< 
* Connection #0 to host api-proxy-rbc left intact
{"list":[{"url":"http://api-zksync-recetas-bc:8000","pattern":"did:recetasbc","behavior":1}]}%
~~~