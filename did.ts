import { ModenaUniversalRegistry } from "@quarkid/did-registry";
import { getOrCreateKeys, resolveDid, QuarkIDEndpoint, saveDid, getDid, kmsFactory, createKeys } from "./common";
import { AssertionMethodPurpose, KeyAgreementPurpose } from "@quarkid/did-core";
import { Service } from '@extrimian/did-core';


export const createDID = async (profileID: string) => {
    console.log("Creating DID for profile: " + profileID + " ...");

    await kmsFactory(profileID);
    const oldDid = await getDid();
    if (oldDid) {
        console.log("DID already exists: ", oldDid);
        return;
    }

    const registry = new ModenaUniversalRegistry();

    let keys = await createKeys();

    let service = {
      id: 'dwn-default',
      type: 'DecentralizedWebNode',
      serviceEndpoint: {
        nodes: ['https://dwn-ssi.buenosaires.gob.ar/']
      }
    };

    const createDidResponse = await registry.createDID({
      updateKeys: [keys.updateKey],
      recoveryKeys: [keys.recoveryKey],
      verificationMethods: [
        {
          id: "bbsbls",
          type: "Bls12381G1Key2020",
          publicKeyJwk: keys.bbsbls,
          purpose: [new AssertionMethodPurpose()],
        },
        {
          id: "didComm",
          type: "X25519KeyAgreementKey2019",
          publicKeyJwk: keys.didComm,
          purpose: [new KeyAgreementPurpose()],
        },
      ],
      services: [service],
    });
    console.log("longDid created: " + JSON.stringify(createDidResponse.longDid));

    const result = await registry.publishDID({
      universalResolverURL: QuarkIDEndpoint,
      didMethod: "did:quarkid",
      createDIDResponse: createDidResponse,
    });

    console.log("result is:", result);
    saveDid(result.did);
    const didDocument = await resolveDid(result.did);
    console.log("Did Document is:", didDocument);
};

const id = process.argv[2];
if (!id) {
  console.log("Usage: node did.ts <profileID>");
  process.exit(1);
}
createDID(id);
