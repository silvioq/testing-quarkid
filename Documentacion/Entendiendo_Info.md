# Informacion de la DB.

## operations

En esta tabla se almacena las operaciones, en este caso una operacion create.

La informacion relevante que se observa es el didSuffix, operationBufferBsonBinary (La operecion create codificada en base64) codificada en base64, txnNumber y txnTime (Bloque).

Para ubicar esta informacion en la tabla transacctions se debe buscar por txnNumber = txnNumber y txnTime = transactionTime. 

```json
{
  "_id": {
    "$oid": "66623a72c3394fb9a556e925"
  },
  "type": "create",
  "didSuffix": "EiBvtFpo7G7YfWbznhM9nAOey4zSidvFh_mW8mNz77zDLg",
  "operationBufferBsonBinary": {
    "$binary": {
      "base64": "eyJ0eXBlIjoiY3JlYXRlIiwic3VmZml4RGF0YSI6eyJkZWx0YUhhc2giOiJFaUNPUlBTaWpvRzNFU1NVc1o5dWZTajNGc3FuNHJjYmlSbVFhQjlrcUdoR1d3IiwicmVjb3ZlcnlDb21taXRtZW50IjpbIkVpQmRVRWFoYnZ0NV82SDk0ck5LMHZHZnEycDJ2QlN5UGxQTUVTUzZBZ2JfT1EiXX0sImRlbHRhIjp7InVwZGF0ZUNvbW1pdG1lbnQiOlsiRWlBYlhwcUFtcFlNRjB3V19US0VwUWV0X1RVMHdHNjlISWNhZWdjQ2JUVW9oUSJdLCJwYXRjaGVzIjpbeyJhY3Rpb24iOiJyZXBsYWNlIiwiZG9jdW1lbnQiOnsicHVibGljS2V5cyI6W3siaWQiOiJiYnNibHMiLCJwdWJsaWNLZXlKd2siOnsia3R5IjoiRUMiLCJjcnYiOiJCbHMxMjM4MUcyS2V5MjAyMCIsIngiOiJzczNRSEZzYVV3NmRQWWU0YVhEMV9KYkozTDdmZ2stNUZqQzdoR3F6cXU1amJvUlNoWmx5eVJjMmlfd2VxVi1XIiwieSI6IkR2eWphZ2x0dHB5RDFhS0ZobFplLTN2QlVfdHJnODJ5U2txNy02ZFZSSWlXQ0M5X2FlMFhCTnZyQ3k5VlFQSGcifSwidHlwZSI6IkJsczEyMzgxRzFLZXkyMDIwIiwicHVycG9zZXMiOlsiYXNzZXJ0aW9uTWV0aG9kIl19LHsiaWQiOiJkaWRDb21tIiwicHVibGljS2V5SndrIjp7Imt0eSI6IkVDIiwiY3J2IjoiZWQyNTUxOSIsIngiOiJIUzlSanZnbC1fYmpWVDRFbm4tUHFnIiwieSI6Ing0WXItRTdTNi0yS2hRMXhkNDVHQ3cifSwidHlwZSI6IlgyNTUxOUtleUFncmVlbWVudEtleTIwMTkiLCJwdXJwb3NlcyI6WyJrZXlBZ3JlZW1lbnQiXX1dLCJzZXJ2aWNlcyI6W3siaWQiOiJkd24tZGVmYXVsdCIsInR5cGUiOiJEZWNlbnRyYWxpemVkV2ViTm9kZSIsInNlcnZpY2VFbmRwb2ludCI6eyJub2RlcyI6WyJodHRwczovL2R3bi1zc2kuYnVlbm9zYWlyZXMuZ29iLmFyLyJdfX1dfX1dfX0=",
      "subType": "00"
    }
  },
  "opIndex": 0,
  "txnNumber": {
    "$numberLong": "14027"
  },
  "txnTime": 2731166
}
```

## transaction.


```json

{
  "_id": {
    "$oid": "66623a40f77aa4001f3678e4"
  },
  "anchorString": "1.Qmd9qEjWmi1FeY5KyzQiGsbKcEL6DvtTp6Ws2VVr2bALme",
  "txnNumber": {
    "$numberLong": "14027"
  },
  "transactionTime": 2731166,
  "transactionTimeHash": "0x32167c8b87827d0f7eb966674e803cd18079ad8693e6fe3e2cfd2f40ddf6e84e",
  "transactionFeePaid": 0,
  "normalizedTransactionFee": 0,
  "writer": "0x09ffEe3e51F0Bea294220ABe19f7dFB2a5D7FB6D"
}
```

### Descripción de los Campos

* _id:
  * Identificador único del documento en la base de datos MongoDB, representado por un ObjectId que es un valor hexadecimal único generado automáticamente por MongoDB.
* anchorString:
  * Se refiere a un hash IPFS. La cadena "1 Qmd9qEjWmi1FeY5KyzQiGsbKcEL6DvtTp6Ws2VVr2bALme" combina un prefijo y un hash IPFS, indicando una versión específica del contenido anclado en la blockchain.
* txnNumber:
  * Número de transacción asociado a este anclaje específico. Representa un identificador único para la transacción en el contexto del sistema zkSync.
* transactionTime:
  * Representa el número de bloque.
* transactionTimeHash:
  * Hash de la transacción, que proporciona una prueba criptográfica del momento en que se realizó la transacción. **(En el explorador de bloque figura como 0x1ea0bb962433353b235decd45d14b1aece4df12f0d12cff90dee30a46afbb4b3)** SEGUIR INVESTIGANDO ESTE TEMA.
* transactionFeePaid:
  * Indica la tarifa de transacción pagada por esta operación específica. 
* normalizedTransactionFee:
  * Descripción: Indica una tarifa de transacción
* writer:
  * Es una dirección de Ethereum que identifica al usuario o entidad que realizó la operación.

## Log del contenedor api-zksync.

> Siguiendo la informacion obtenida en los puntos de mas arriba lo identificaremos con el log del contenedor.

* Buscamos en el log del contenedor el **didSuffix**, de esta manera encontramos el evento **create** y la conexion a la base de datos.

```
Operation type: 'create', DID unique suffix: 'EiBvtFpo7G7YfWbznhM9nAOey4zSidvFh_mW8mNz77zDLg'
processing a did, going trough state:undefinedtype: create
CommandResult {
  result: { n: 1, ok: 1 },
  connection: Connection {
    _events: [Object: null prototype] {
      error: [Function (anonymous)],
      close: [Function (anonymous)],
      timeout: [Function (anonymous)],
      parseError: [Function (anonymous)],
      message: [Function (anonymous)]
    },
    _eventsCount: 5,
    _maxListeners: undefined,
    id: 4,
    options: {
      host: 'mongo-quarkid',
      port: 27017,
            ...........
```
* Luego observanmos el status **succeeded** y el **didDocument** 

```json
{
  status: 'succeeded',
  body: {
    '@context': 'https://w3id.org/did-resolution/v1',
    didDocument: {
      '@context': [Array],
      id: 'did:recetasbc:EiBvtFpo7G7YfWbznhM9nAOey4zSidvFh_mW8mNz77zDLg',
      controller: undefined,
      verificationMethod: [Array],
      assertionMethod: [Array],
      keyAgreement: [Array],
      service: [Array]
    },
    didDocumentMetadata: {
      method: [Object],
      versionId: '1',
      canonicalId: 'did:recetasbc:EiBvtFpo7G7YfWbznhM9nAOey4zSidvFh_mW8mNz77zDLg'
    }
  }
}
Handling resolution request for: did:recetasbc:EiBvtFpo7G7YfWbznhM9nAOey4zSidvFh_mW8mNz77zDLg...
Resolving DID unique suffix 'EiBvtFpo7G7YfWbznhM9nAOey4zSidvFh_mW8mNz77zDLg'...
intial state created
DID not found for DID 'did:recetasbc:EiBvtFpo7G7YfWbznhM9nAOey4zSidvFh_mW8mNz77zDLg'...
Start operation batch writing...
```
* El **didDocument** se almacena en la red del **IPFS** 

> Entiendo que a CID le saca un hash y este queda registrado en la Blockchain, dentro de la transaccion en data.

```
Wrote 240 byte content as IPFS CID: Qmd9qEjWmi1FeY5KyzQiGsbKcEL6DvtTp6Ws2VVr2bALme
Writing data to blockchain: 1.Qmd9qEjWmi1FeY5KyzQiGsbKcEL6DvtTp6Ws2VVr2bALme with minimum fee of: 0
```

* Confirmacion de la Transaccion.

```
Transaction successful: {
  nonce: 36,
  gasPrice: BigNumber { _hex: '0x017d7840', _isBigNumber: true },
  gasLimit: BigNumber { _hex: '0x03777e', _isBigNumber: true },
  to: '0x2535412fA22D9ad83384D7Ab7b636DDA37eFA872',
  value: BigNumber { _hex: '0x00', _isBigNumber: true },
  data: '0x4cd27ad5dc19d1b0d84efd761064dfa6afbe3ec061d64e0da044e3d3bb41344c80dc4f310000000000000000000000000000000000000000000000000000000000000001',
  chainId: 300,
  v: 636,
  r: '0xa4270c705c6418546993622a1bff0605b3e6d37e5a938446dbf94e5994f6297c',
  s: '0x6b26fef66d8a9e81d91da3277219b8267303b8af57f8a2f4799152bb40dab17e',
  from: '0x09ffEe3e51F0Bea294220ABe19f7dFB2a5D7FB6D',
  hash: '0x1ea0bb962433353b235decd45d14b1aece4df12f0d12cff90dee30a46afbb4b3',
  type: null,
  confirmations: 0,
  wait: [Function (anonymous)],
  waitFinalize: [AsyncFunction (anonymous)]
}
```