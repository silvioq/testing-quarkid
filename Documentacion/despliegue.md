# Despliegue de la Infraestructura

> En esta primera etapa de investigación y desarrollo, el despliegue es artesanal, ya que no se contempló automatizarlo.

> Para poder iniciar la infraestructura y comenzar a transaccionar en ella, es necesario realizar algunas configuraciones.

## Cómo crear la wallet

> Lo primero que tenemos que saber es en qué red vamos a transaccionar. En este caso, usaremos **https://sepolia.era.zksync.dev**.

> Descargar e instalar **MetaMask** para conectarlo a la red que se utilizará para generar las claves públicas y privadas de nuestra wallet. Puedes encontrar más información en [este enlace](https://support.metamask.io/getting-started/getting-started-with-metamask/).

> Utilizar un **Faucet** para transferir **ETH** y poder transaccionar. **(Agregar link)**

## Cómo iniciar los servicios

### Introducción

> Para iniciar los servicios se disponibiliza un archivo **docker-compose.yml** el cual genera una red Docker y los volúmenes necesarios para persistir los datos.

> Editar el archivo **/etc/hosts** para la resolución de nombres de los contenedores. **(Agregar Ejemplo)** 

### Imágenes disponibilizadas

* **api-zksync-recetas-bc**: utiliza la imagen subida a Docker Hub [ver enlace](https://hub.docker.com/r/quarkid/api-zksync). De ser necesario, se puede generar una imagen personalizada siguiendo las instrucciones del proyecto [api-zkSync](https://github.com/gcba/api-zkSync/tree/master/source).

* **api-proxy-rbc**: utiliza la imagen subida a Docker Hub [ver enlace](https://hub.docker.com/r/quarkid/api-proxy). En este caso se utilizó una imagen personalizada ya que se reconfiguró el servicio de **api-proxy** a la infraestructura local que se montó. [api-proxy](https://gitlab.com/silvioq/testing-quarkid/-/tree/main/infra-recetasbc/Componentes/api-proxy-recetasbc)

    * Archivos modificados para la generación de la imagen:

> [config.json](https://gitlab.com/silvioq/testing-quarkid/-/blob/main/infra-recetasbc/Componentes/api-proxy-recetasbc/source/enviroments/config.json)

```json
{
  "NODE_1_URL": "http://api-zksync-recetas-bc:8000",
  "NODE_1_PATTERN": "did:recetasbc",
  "NODE_1_BEHAVIOR": 1
}
```
> [.env](https://gitlab.com/silvioq/testing-quarkid/-/blob/main/infra-recetasbc/Componentes/api-proxy-recetasbc/source/src/.env)

```
NODE_1_URL='http://api-zksync-recetas-bc:8000'
NODE_1_PATTERN='did:recetasbc'
NODE_1_BEHAVIOR='1'
TZ='America/Argentina/Buenos_Aires'
```

> Como construir la imagen.

* Clonar el repo.
    
    ```
    git clone https://gitlab.com/silvioq/testing-quarkid/
    ```

    *   Ubicarse en el siguiente directorio **infra-recetasbc/Componentes/api-proxy-recetasbc/source/src**

    ```
    docker build -t api-prory-recetasbc:v0.0.2 .
    ```

* **mongo-quarkid**: Utiliza la imagen oficial de docker-hub.

* **ipfs**: Utiliza la imagen oficial de docker-hub.

### Explicación del Docker Compose

> Al ejecutar el comando **docker-compose up -d**, el servicio **api-zksync-recetas-bc** se conecta con la **Blockchain**, a la **base de datos** y a la red **IPFS** con las definiciones configurables en el archivo **.env**.

> A su vez, tanto la base de datos como el IPFS persisten los datos en un volumen local.

> También se inicia el **api-proxy-rbc**, que interactúa directamente entre el frontend y el **api-zksync-recetas-bc**, habilitando los endpoints para la creación y resolución de DIDs.