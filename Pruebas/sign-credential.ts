import { DWNClient, MessageStorage, ThreadMethod } from "@quarkid/dwn-client";
import { Entry } from '@quarkid/dwn-client/dist/types/message';
import { buildCredential, getDid, getOrCreateKeys, kmsFactory, resolveDid } from "./common";
import { IDIDCommMessage } from "@extrimian/kms-core";
import { VerifiableCredentialService } from "@quarkid/vc-core";
import { Suite } from "@quarkid/kms-core";
import { AssertionMethodPurpose, DIDDocumentUtils, VerificationMethodJwk } from "@quarkid/did-core";
import { KMSClient } from "@quarkid/kms-client";

import {
    BaseConverter,
    Base,
  } from "@extrimian/kms-core";


const buildMe = async () => {
    await kmsFactory(profileID);
    const vc = await buildCredential();
    console.log("vc signed: ", vc);
    return vc;
};


// tomo el profile de argumentos de consola
const profileID = process.argv[2];
if (!profileID) {
    console.log("Usage: npx tsx sign-credential.ts <profileID>");
    process.exit(1);
}

buildMe();

