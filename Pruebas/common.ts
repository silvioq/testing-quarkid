import { KMSClient } from "@quarkid/kms-client"
import { IJWK, LANG, Suite } from "@quarkid/kms-core"
import { FileSystemKMSSecureStorage } from "./storage"
import { AssertionMethodPurpose, DIDDocument } from "@quarkid/did-core";
import { DIDDocumentMetadata, DIDUniversalResolver } from "@quarkid/did-resolver";
import { VerifiableCredential, VerifiableCredentialService } from "@quarkid/vc-core";
import { VCVerifierService } from "@quarkid/vc-verifier";

export const QuarkIDEndpoint = "http://3.213.251.204:8080";

export const resolveDid = (did: string) =>
    new Promise<DIDDocument>((resolve, reject) => {
      const universalResolver = new DIDUniversalResolver({
        universalResolverURL: QuarkIDEndpoint,
      });
  
      universalResolver.resolveDID(did).then((didDocument) => {
        resolve(didDocument);
      }).catch((error) => {
        reject(error);
      })
    })
    ;

let kms: null | KMSClient = null;
let kmsStorage: FileSystemKMSSecureStorage;
export const kmsFactory = async (id:string) => {
    if (kms) {
        return kms;
    }
    return kms = new KMSClient({
        lang: LANG.en,
        storage: kmsStorage = new FileSystemKMSSecureStorage({
            filepath: "file-system-storage." + id + ".json"
        }),
        didResolver: resolveDid
    })
};

export const verify = async (vc: VerifiableCredential) => {
    if (!kms) {
        console.log("No KMS initialized. Please, run kmsFactory first.");
        process.exit(1);
    }
    const service = new VCVerifierService({
        didDocumentResolver: resolveDid
    });
    const result = await service.verify(vc, new AssertionMethodPurpose());
    return result;
};

export const buildCredential = async () => {
    if (!kms) {
        console.log("No KMS initialized. Please, run kmsFactory first.");
        process.exit(1);
    }
    const vcService = new VerifiableCredentialService();
    const didEmisor = await getDid();

    if (!didEmisor) {
        console.log("No DID found for profile.");
        process.exit(1);
    }

    const credential = await vcService.createCredential({
      context: [
        "https://w3id.org/vaccination/v1",
        "https://w3id.org/security/v2",
        "https://w3id.org/security/bbs/v1",
      ],
      vcInfo: {
        issuer: didEmisor,
        expirationDate: new Date("2026/05/05"),
        id: "123456789",
        types: ["VaccinationCertificate"],
      },
      data: {
        type: "VaccinationEvent",
        batchNumber: "1183738569",
        administeringCentre: "MoH",
        healthProfessional: "MoH",
        countryOfVaccination: "NZ",
        recipient: {
          type: "VaccineRecipient",
          givenName: "GUSTAVO",
          familyName: "FASCOWICZ",
          gender: "Male",
          birthDate: "1958-07-17",
        },
        vaccine: {
          type: "Vaccine",
          disease: "COVID-19",
          atcCode: "J07BX03",
          medicinalProductName: "COVID-19 Vaccine Moderna",
          marketingAuthorizationHolder: "Moderna Biotech",
        },
      },
      mappingRules: null,
    });

    let allKeys = await getKeys();
    const bbsbls2020:IJWK = allKeys.bbsbls;
    
    const vc = await kms.signVC(
      Suite.Bbsbls2020,
      bbsbls2020,
      credential,
      didEmisor,
      didEmisor + "#bbsbls",
      new AssertionMethodPurpose()
    );

    return vc;
};

export type LocalPublicKeys = {
    updateKey: IJWK,
    recoveryKey: IJWK,
    didComm: IJWK,
    bbsbls: IJWK
};


export const saveDid =  async (did: string ) => {
    await kmsStorage.add("did", did);
}

export const getDid: () => Promise<string|null> = async () => {
    return await kmsStorage.get("did");
}

export const getKeys: () => Promise<LocalPublicKeys> = async () => {
    if (!kms) {
        console.log("No KMS initialized. Please, run kmsFactory first.");
        process.exit(1);
    }
    let allKeys = await kms.getAllPublicKeys();
    if (allKeys.length === 0) {
        console.log("No keys found. Please, run getOrCreateKeys first.");
        throw new Error("No keys found");
    }
    return {
        updateKey: allKeys[0],
        recoveryKey: allKeys[1],
        didComm: allKeys[2],
        bbsbls: allKeys[3]
    }
}

export const createKeys: () => Promise<LocalPublicKeys> = async () => {
    if (!kms) {
        console.log("No KMS initialized. Please, run kmsFactory first.");
        process.exit(1);
    }
    const updateKey = await kms.create(Suite.ES256k);
    const recoveryKey = await kms.create(Suite.ES256k);
    const didComm = await kms.create(Suite.DIDComm);
    const bbsbls = await kms.create(Suite.Bbsbls2020);
    
    return {
        updateKey: updateKey.publicKeyJWK,
        recoveryKey: recoveryKey.publicKeyJWK,
        didComm: didComm.publicKeyJWK,
        bbsbls: bbsbls.publicKeyJWK
    };
}

export const getOrCreateKeys: (id:string) => Promise<LocalPublicKeys> = async (id:string) => {
    const kms = await kmsFactory(id);
    return getKeys().then(keys => {
        return keys;
    }).catch(async () => {
        return await createKeys();
    });
}

