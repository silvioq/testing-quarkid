import { DIDDocumentUtils } from "@quarkid/did-core";
import { kmsFactory, resolveDid, getDid } from "./common";

const getDidFromParams = async (didOrProfile:string) => {
    let did:string|null = null;
    // chequeo si tiene la forma de un did
    if (didOrProfile.startsWith("did:")) {
        did = didOrProfile;
    } else {
        await kmsFactory(didOrProfile);
        did = await getDid();
    }

    if (null === did) {
        console.log("No DID found");
        console.log("Usage: node resolve.ts <did|profileID>");
        process.exit(1);
    }

    return did;
}

const resolveMe = async (didOrProfile:string) => {
    const did = await getDidFromParams(didOrProfile)
    const result = await resolveDid(did);
    console.log("result from resolveDid is ...", result);

    if (result && !result.service) {
        console.log("No service found in DID Document");
        return;
    } else if (result) {
        result.service?.forEach(service => {
            console.log("service", service);
        });
    }
    //console.log(DIDDocumentUtils.getServiceUrl(result, "did-communication"));
}

// tomo el did de los argumentos de consola
const didOrProfile = process.argv[2];

if (!didOrProfile) {
    console.log("Usage: node resolve.ts <did|profileID>");
    process.exit(1);
}

resolveMe(didOrProfile);
