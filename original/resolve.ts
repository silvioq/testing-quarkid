import { FileSystemKMSSecureStorage } from "./storage";
import { KMSClient } from "@quarkid/kms-client";
import { LANG, Suite } from "@quarkid/kms-core";
import { ModenaUniversalRegistry } from "@quarkid/did-registry";
import { AssertionMethodPurpose, KeyAgreementPurpose } from "@quarkid/did-core";
import { DIDUniversalResolver } from "@quarkid/did-resolver";
import { DIDDocument } from "@quarkid/did-core";

export const QuarkIDEndpoint = "https://node-ssi.buenosaires.gob.ar";
export const did = "did:quarkid:EiC6l15qLkhYxzsFckBztgaLnP-_djZxQXDZBUvft_qrUg"; // esto lo obtuve por consola

export const resolveDid = (did: string) =>
    new Promise<DIDDocument>((resolve, reject) => {
      console.log("Resolving DID");
      setTimeout(async () => {
        const universalResolver = new DIDUniversalResolver({
          universalResolverURL: QuarkIDEndpoint,
        });
  
        console.log("In resolveDid, calling resolveDID with did:", did);
  
        const didDocument = await universalResolver.resolveDID(did);
        console.log("In resolveDid, didDocument is:", didDocument);
        return didDocument;
      }, /* 65000 */ 1000); // la espera original era de 65 segundos, la bajo a uno, no sé por qué estaba tan alta
    }); 
  

const resolveMe = async () => {
    const result = await resolveDid(did);
    console.log("result from resolveDid is ...", result);
}

resolveMe();

