import { VerifiableCredentialService } from "@quarkid/vc-core";
import { KMSClient } from "@quarkid/kms-client";
import { LANG, Suite } from "@quarkid/kms-core";
import { AssertionMethodPurpose } from "@quarkid/did-core";
import { FileSystemKMSSecureStorage } from "./storage";
import { VCVerifierService } from "@quarkid/vc-verifier";
import { DIDUniversalResolver } from "@quarkid/did-resolver";
import { DIDDocument } from "@quarkid/did-core";

const QuarkIDEndpoint = "https://node-ssi.buenosaires.gob.ar";
const did = "did:quarkid:EiC6l15qLkhYxzsFckBztgaLnP-_djZxQXDZBUvft_qrUg"; // esto lo obtuve por consola

export const resolveDid = (did: string) =>
    new Promise<DIDDocument>((resolve, reject) => {
      console.log("Resolving DID");
      setTimeout(async () => {
        const universalResolver = new DIDUniversalResolver({
          universalResolverURL: QuarkIDEndpoint,
        });
  
        console.log("In resolveDid, calling resolveDID with did:", did);
  
        const didDocument = await universalResolver.resolveDID(did);
        console.log("In resolveDid, didDocument is:", didDocument);
        resolve(didDocument);
      }, /* 65000 */ 1000); // la espera original era de 65 segundos, la bajo a uno, no sé por qué estaba tan alta
    }); 
  

const credential = async () => {
  const vcService = new VerifiableCredentialService();

  const credential = await vcService.createCredential({
    context: [
      "https://w3id.org/vaccination/v1",
      "https://w3id.org/security/v2",
      "https://w3id.org/security/bbs/v1",
    ],
    vcInfo: {
      issuer: did,//"did:quarkid:EiBA3ihJrI5fSsdpZWd3H_-0Wr4rEL8muoDOsuDQDhe2FQ",
      expirationDate: new Date("2026/05/05"),
      id: "123456789",
      types: ["VaccinationCertificate"],
    },
    data: {
      type: "VaccinationEvent",
      batchNumber: "1183738569",
      administeringCentre: "MoH",
      healthProfessional: "MoH",
      countryOfVaccination: "NZ",
      recipient: {
        type: "VaccineRecipient",
        givenName: "JOHN",
        familyName: "SMITH",
        gender: "Male",
        birthDate: "1958-07-17",
      },
      vaccine: {
        type: "Vaccine",
        disease: "COVID-19",
        atcCode: "J07BX03",
        medicinalProductName: "COVID-19 Vaccine Moderna",
        marketingAuthorizationHolder: "Moderna Biotech",
      },
    },
    mappingRules: null,
  });

  console.log("Credential", credential);

  const kms = new KMSClient({
    lang: LANG.es,
    storage: new FileSystemKMSSecureStorage({
      filepath: "file-system-storage",
    }),
    didResolver: resolveDid
  });

  const bbsbls2020 = await kms.getPublicKeysBySuiteType(Suite.Bbsbls2020);
  console.log(bbsbls2020);
  bbsbls2020.forEach(element => {
    console.log(element);
  });

  return;

  const vc = await kms.signVC(
    Suite.Bbsbls2020,
    bbsbls2020[0],
    credential,
    did,
    did + "#bbsbls",
    new AssertionMethodPurpose()
  );
  console.log("Verifiable Credential Signed", vc);

  const service = new VCVerifierService({
    didDocumentResolver: async (did: string) => {
        const universalResolver = new DIDUniversalResolver({
          universalResolverURL: QuarkIDEndpoint,
        });
    
        const didDocument = await universalResolver.resolveDID(did);
    
        return didDocument;
    }
  });
  const result = await service.verify(vc, new AssertionMethodPurpose());
  console.log("result", result);
  
};
credential();




