
# zkSync Blockchain Query Tool

## Descripción

Esta herramienta permite consultar la blockchain de zkSync en sus entornos de prueba y producción, mostrando los resultados de las búsquedas de manera sencilla. Utiliza llamadas JSON-RPC para obtener diversos tipos de información de la blockchain y formatea las respuestas para una fácil lectura.

## Requisitos

- `curl`
- `jq`
- `bc`

## Uso

1. Clona este repositorio o guarda el script en tu máquina local.
2. Dale permisos de ejecución al script:
   ```bash
   chmod +x zksync_bc_query_tool.sh
   ```
3. Ejecuta el script:
   ```bash
   ./zksync_bc_query_tool.sh
   ```

## Funcionalidades

El script permite realizar las siguientes consultas a la blockchain de zkSync:

1. **Balance de una cuenta**: Muestra el balance en ETH.
2. **Bloque por número (decimal)**: Consulta un bloque utilizando su número en formato decimal.
3. **Bloque por número (hexadecimal)**: Consulta un bloque utilizando su número en formato hexadecimal.
4. **Bloque por hash**: Consulta un bloque utilizando su hash.
5. **Código de un contrato**: Obtiene el código de un contrato inteligente.
6. **Número del bloque actual**: Muestra el número del bloque actual en formato decimal y hexadecimal.
7. **Transacción por hash**: Consulta una transacción utilizando su hash.
8. **Versión del cliente web3**: Obtiene la versión del cliente web3.
9. **Versión del protocolo eth**: Obtiene la versión del protocolo Ethereum.

## Instrucciones de Uso

1. Al ejecutar el script, se te pedirá que selecciones el entorno de la blockchain:
   - `1` para zkSync Mainnet (producción)
   - `2` para zkSync Testnet (prueba)

2. Luego, selecciona el tipo de búsqueda que deseas realizar:
   - `1` para Balance de una cuenta
   - `2` para Bloque por número (decimal)
   - `3` para Bloque por número (hexadecimal)
   - `4` para Bloque por hash
   - `5` para Código de un contrato
   - `6` para Número del bloque actual
   - `7` para Transacción por hash
   - `8` para Versión del cliente web3
   - `9` para Versión del protocolo eth

3. Según el tipo de búsqueda seleccionada, se te pedirá que ingreses el valor necesario (dirección de cuenta, número de bloque, hash de transacción, etc.).

4. El script realizará la consulta correspondiente y mostrará el resultado en la consola.

## Ejemplo

Para consultar una transacción por hash en la red de producción:

```bash
./zksync_bc_query_tool.sh
```

Selecciona `1` para zkSync Mainnet y luego `7` para Transacción por hash. Ingresa el hash de la transacción cuando se te pida y verás el resultado detallado de la transacción en formato JSON.

```bash
➜  tool git:(main) ./zksync_bc_query_tool.sh
jq está instalado.
curl está instalado.
bc está instalado.
Seleccione el entorno:
1) Producción (zkSync Mainnet)
2) Prueba (zkSync Testnet)
Opción: 1
Seleccione el tipo de búsqueda:
1) Balance de una cuenta
2) Bloque por número (decimal)
3) Bloque por número (hexadecimal)
4) Bloque por hash
5) Código de un contrato
6) Número del bloque actual
7) Transacción por hash
8) Versión del cliente web3
9) Versión del protocolo eth
Opción: 7
Ingrese el valor (número de bloque/tx hash/dirección de contrato/etc.): 0x318252b370f080ccc15202de4119825ccb4e0e7fe0934bdcd74a830778076a0a
```
~~~json
{
  "jsonrpc": "2.0",
  "result": {
    "blockHash": "0x7e767c0f7273c18a571eb78cbadd0020316b8787988fa3e1b0a433bd41a2ee9e",
    "blockNumber": "0x2603aa7",
    "chainId": "0x144",
    "from": "0x232e65c20af532344e4ea79cb0cdb15a9b5f995b",
    "gas": "0x26999",
    "gasPrice": "0x2b275d0",
    "hash": "0x318252b370f080ccc15202de4119825ccb4e0e7fe0934bdcd74a830778076a0a",
    "input": "0x4cd27ad5ebb92a6f4cadb9c0c308b494af14f4f07d8622d68ab71b8d1397d4e4e402b9810000000000000000000000000000000000000000000000000000000000000001",
    "l1BatchNumber": "0x77851",
    "l1BatchTxIndex": "0xdb0",
    "maxFeePerGas": "0x2b275d0",
    "maxPriorityFeePerGas": "0x2b275d0",
    "nonce": "0xe",
    "r": "0x58c0fb1a1f196cb5f565f2fc3197476bbabcbdfc93c8e8cadac12ea7c2f32df",
    "s": "0x2472244c6db26ce13b1deac08bfdf1224d3e7fb5b7b772103670bf262a795797",
    "to": "0xe0055b74422bec15cb1625792c4aa0bedcc61aa7",
    "transactionIndex": "0x1",
    "type": "0x0",
    "v": "0x2ac",
    "value": "0x0"
  },
  "id": 1
}
~~~

## Notas

- Asegúrate de tener instalados `curl`, `jq` y `bc` en tu sistema. Si no están instalados, puedes hacerlo ejecutando:
  ```bash
  sudo apt-get install -y jq curl bc
  ```
- La herramienta está diseñada para ser simple y fácil de usar, facilitando la consulta de la blockchain de zkSync.

