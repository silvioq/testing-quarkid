#!/bin/bash

# Función para verificar si los comandos necesarios están instalados
verificar_comandos() {
  for cmd in jq curl bc; do
    if ! command -v $cmd &> /dev/null; then
      echo "Error: $cmd no está instalado. Por favor, instale $cmd antes de continuar."
      DEPENDENCIAS_OK=0
    else
      echo "$cmd está instalado."
    fi
  done
}

# Verificar los comandos necesarios
DEPENDENCIAS_OK=1
verificar_comandos

if [ $DEPENDENCIAS_OK -eq 0 ]; then
  echo "Por favor, instale las dependencias necesarias y vuelva a ejecutar el script."
  echo "Para instalar las dependencias, ejecute:"
  echo "sudo apt-get install -y jq curl bc"
  exit 1
fi

# Función para mostrar el menú y obtener la elección del usuario
mostrar_menu() {
  echo "Seleccione el entorno:"
  echo "1) Producción (zkSync Mainnet)"
  echo "2) Prueba (zkSync Testnet)"
  read -p "Opción: " ENTORNO_OPCION

  echo "Seleccione el tipo de búsqueda:"
  echo "1) Balance de una cuenta"
  echo "2) Bloque por número (decimal)"
  echo "3) Bloque por número (hexadecimal)"
  echo "4) Bloque por hash"
  echo "5) Código de un contrato"
  echo "6) Número del bloque actual"
  echo "7) Transacción por hash"
  echo "8) Versión del cliente web3"
  echo "9) Versión del protocolo eth"
  read -p "Opción: " TIPO_OPCION
}

# Mostrar el menú
mostrar_menu

# Definir la URL del endpoint basado en el entorno seleccionado
case $ENTORNO_OPCION in
  1)
    URL="https://mainnet.era.zksync.io"
    echo "Red seleccionada: Producción (zkSync Mainnet)"
    ;;
  2)
    URL="https://sepolia.era.zksync.dev"
    echo "Red seleccionada: Prueba (zkSync Testnet)"
    ;;
  *)
    echo "Entorno no válido. Seleccione una opción válida."
    exit 1
    ;;
esac

echo "URL de consulta: $URL"

# Obtener el argumento si se requiere
if [[ $TIPO_OPCION -eq 1 ]] || [[ $TIPO_OPCION -eq 2 ]] || [[ $TIPO_OPCION -eq 3 ]] || [[ $TIPO_OPCION -eq 4 ]] || [[ $TIPO_OPCION -eq 5 ]] || [[ $TIPO_OPCION -eq 7 ]]; then
  read -p "Ingrese el valor (número de bloque/tx hash/dirección de contrato/etc.): " ARGUMENTO
fi

# Definir el contenido de la solicitud en formato JSON según el tipo seleccionado
case $TIPO_OPCION in
  1)
    JSON_DATA='{"jsonrpc":"2.0","method":"eth_getBalance","params":["'"${ARGUMENTO}"'", "latest"],"id":1}'
    ;;
  2)
    BLOQUE_HEX=$(printf "0x%X" $ARGUMENTO)
    JSON_DATA='{"jsonrpc":"2.0","method":"eth_getBlockByNumber","params":["'"${BLOQUE_HEX}"'", false],"id":1}'
    ;;
  3)
    JSON_DATA='{"jsonrpc":"2.0","method":"eth_getBlockByNumber","params":["'"${ARGUMENTO}"'", false],"id":1}'
    ;;
  4)
    JSON_DATA='{"jsonrpc":"2.0","method":"eth_getBlockByHash","params":["'"${ARGUMENTO}"'", false],"id":1}'
    ;;
  5)
    JSON_DATA='{"jsonrpc":"2.0","method":"eth_getCode","params":["'"${ARGUMENTO}"'","latest"],"id":1}'
    ;;
  6)
    JSON_DATA='{"jsonrpc":"2.0","method":"eth_blockNumber","params":[],"id":1}'
    ;;
  7)
    JSON_DATA='{"jsonrpc":"2.0","method":"eth_getTransactionByHash","params":["'"${ARGUMENTO}"'"],"id":1}'
    ;;
  8)
    JSON_DATA='{"jsonrpc":"2.0","method":"web3_clientVersion","params":[],"id":1}'
    ;;
  9)
    JSON_DATA='{"jsonrpc":"2.0","method":"eth_protocolVersion","params":[],"id":67}'
    ;;
  *)
    echo "Tipo no válido. Seleccione una opción válida."
    exit 1
    ;;
esac

# Ejecutar la solicitud curl y procesar la salida
if [[ $TIPO_OPCION -eq 1 ]]; then
  RESPONSE=$(curl -s -X POST -H "Content-Type: application/json" --data "${JSON_DATA}" ${URL})
  if [ $? -ne 0 ]; then
    exit 1
  fi
  BALANCE_HEX=$(echo $RESPONSE | jq -r '.result')
  BALANCE_DEC=$(printf "%d" $BALANCE_HEX)
  BALANCE_ETH=$(bc <<< "scale=18; $BALANCE_DEC / 1000000000000000000")
  echo "Balance: $BALANCE_ETH ETH"
elif [[ $TIPO_OPCION -eq 6 ]]; then
  RESPONSE=$(curl -s -X POST -H "Content-Type: application/json" --data "${JSON_DATA}" ${URL})
  if [ $? -ne 0 ]; then
    exit 1
  fi
  BLOCK_NUMBER_HEX=$(echo $RESPONSE | jq -r '.result')
  BLOCK_NUMBER_DEC=$(printf "%d" $BLOCK_NUMBER_HEX)
  echo "Número de bloque actual: $BLOCK_NUMBER_DEC (decimal), $BLOCK_NUMBER_HEX (hexadecimal)"
else
  curl -X POST -H "Content-Type: application/json" --data "${JSON_DATA}" ${URL} | jq .
  if [ $? -ne 0 ]; then
    exit 1
  fi
fi

exit 0
