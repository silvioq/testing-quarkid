## Instalación

```bash
npm install
```

## Profiles
Todas las claves públicas y privadas de los perfiles se guardan en los
archivos `file-system-storage.<ProfileID>.json`. Estos archivos se generan
con el comando `npx tsx did.ts <ProfileID>`.

## Generar un DID
    
```bash
npx tsx did.ts profileID
```

Esto genera un DID en la consola. El DID tarda un minuto en "resolverse".
Antes de generar una credencial hay que chequear vía "resolve.ts"

## Resolver un DID

```bash
npx tsx resolve.ts <DID o ProfileID>
```

Si el DID no está resuelto, devuelve "nulo".

## Generar una credencial

```bash
npx tsx sign-credential.ts <ProfileID>
```
Esto genera una credencial en la consola. Inmediatamente se verifica.

## Enviar una credencial

```bash
npx tsx send-credential.ts <ProfileID> <TargetDID>
```

## Ejemplo completo

¡¡OBSOLETO!!

[![asciicast](https://asciinema.org/a/AegVVI1D6eRrZxizMm2z7YkhU.svg)](https://asciinema.org/a/AegVVI1D6eRrZxizMm2z7YkhU)